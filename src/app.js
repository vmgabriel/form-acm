import express from 'express';
import path from 'path';
import logger from 'morgan';
import bodyParser from 'body-parser';
import routes from './routes';
import mongoose from 'mongoose';

// Configuracion de la base de datos
import configConection from '../config/connection';

// V0 routes
import v0 from './v0/routes';

const app = express();

app.disable('x-powered-by');

// View engine setup
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'pug');

app.use(logger('dev', {
  skip: () => app.get('env') === 'test'
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '../public')));

// Conexion con la base de datos
mongoose.connect(configConection.uri, configConection.options);

mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + configConection.uri);
});

// Routes
app.use('/', routes);

// V0 routes
app.use('/v0', v0);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handler
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  res
    .status(err.status || 500)
    .render('error', {
      message: err.message
    });
});

export default app;

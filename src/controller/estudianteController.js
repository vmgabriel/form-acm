import mongoose from 'mongoose';
import Estudiante from '../model/estudiante';

// DAO - Estudiante
// GET - SHOW ESTUDIANTE
function getEstudiante(req, res) {
    return Estudiante.findById(req.params.id).exec();
};

// POST - NEW ESTUDIANTE
function postEstudiante(req, res) {
    let nEst = new Estudiante(req.body);
    return nEst.save();
};

// PUT - UPDATE ESTUDIANTE
function putEstudiante(req, res) {
    let mEst = new Estudiante(req.body);
    console.log(mEst);
    return Estudiante.findOneAndUpdate({_id: req.params.id}, mEst).exec();
};

// DELETE - REMOVE ESTUDIANTE
function deleteEstudiante(req, res) {
    return Estudiante.findByIdAndRemove(req.params.id).exec();
};

// ----
// GET - LIST ESTUDIANTE
function getEstudiantes(req, res) {
    return Estudiante.find().exec();
};

module.exports = { getEstudiantes, getEstudiante, postEstudiante };

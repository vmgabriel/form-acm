import { Router } from 'express';

import ctrlEst from '../controller/estudianteController';

const routes = Router();

routes.get('/estudiantes', (req, res) => {
    ctrlEst.getEstudiantes(req, res)
        .then(doc => res.status(200).json(doc))
        .catch(err => {
            console.error(err);
            res.status(500).json({ error: 'Conexion fallida' });
        });
});

routes.get('/estudiantes/:id', (req, res) => {
    ctrlEst.getEstudiante(req, res)
        .then(doc => res.status(200).json(doc))
        .catch(err => {
            console.error(err);
            res.status(500).json({ error: 'Id no disponible' });
        });
});

routes.post('/estudiantes', (req, res) => {
    ctrlEst.postEstudiante(req, res)
        .then(doc => res.status(201).json(doc))
        .catch(err => {
            console.error(err);
            res.status(500).json({ error: err });
        });
});

export default routes;

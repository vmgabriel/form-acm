import mongoose from 'mongoose';

let Schema = mongoose.Schema;

let estudianteSchema = new Schema({
    nombre1: { type: String, required: true },
    nombre2: { type: String },
    apellido1: { type: String, required: true },
    apellido2: { type: String },
    codigo: { type: Number, required: true },
    cedula: { type: String,
              required: true,
            },
    correo: { type: String,
              required: true,
              validate: {
                  validator: function(v) {
                      return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
                  }
              }},
    celular: { type: Number, required: true },
    fechaCumpleaños: { type: Date },
    nMatricula: { type: Number, required: true },
    cursosInteres: [{ nombre: String }],
    descripcion: String,
    createdAt: { type: Date, default: Date.now }
});

estudianteSchema.pre('save', function () {
    let now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
});

module.exports = mongoose.model('Estudiante', estudianteSchema);

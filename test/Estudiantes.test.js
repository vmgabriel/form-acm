import app from '../src/app.js';

//Modelo
import Estudiante from '../src/model/estudiante';

import chai from 'chai';
import chaiHttp from 'chai-http';

import mongoose from 'mongoose';

import should from 'should';

let expect = chai.expect;

chai.use(chaiHttp);

let tempEst = {
    "nombre1": "J",
    "nombre2": "G",
    "apellido1": "G",
    "apellido2": "M",
    "codigo": 201,
    "correo": "vm@gmail.com",
    "celular": 314,
    "cedula": "100",
    "fechaCumpleaños": "1996-10-09T00:00:00.511Z",
    "nMatricula": 10,
    "cursosInteres": [{"nombre": "GIWEB"}],
    "descripcion": "esto es una descripcion"
};

// DAO ESTUDIANTE

// Crear
describe('POST /v0/estudiantes', () => {
    it('should return 200', () => {
        chai.request(app)
            .post('/v0/estudiantes')
            .send(tempEst)
            .then(res => {
                expect(res).to.have.status(201);
                should.exist(res.body);
            })
            .catch(err => { throw err; });
    });

    it('should return 500 for values no correct', () => {
        chai.request(app)
            .post('/v0/estudiantes')
            .send({})
            .then(res => { throw res; })
            .catch(err => {
                expect(res).to.have.status(500);
                should.exist(res.body.error);
            });
        chai.request(app)
            .post('/v0/estudiantes')
            .send({ "cedula": "10332234" })
            .then(res => { throw res; })
            .catch(err => {
                expect(err).to.have.status(500);
                should.exist(err.body.error);
            });
    });

    it('should return 500 for validation', () => {
        tempEst.codigo = "aabbbaa";
        chai.request(app)
            .post('/v0/estudiantes')
            .send(tempEst)
            .then(res => { throw res; })
            .catch(err => {
                expect(err).to.have.status(500);
                should.exist(err.body.error);
            });
    });
});

// Mostrar
describe('GET /v0/estudiantes/:id', () => {
    let est = new Estudiante(tempEst);
    it('should return 200', () => {
        try {
            est.save()
                .then(estudiante => {
                    chai.request(app)
                        .get('/v0/estudiantes/'+estudiante._id)
                        .then(res => {
                            expect(res).to.have.status(200);
                            should.exist(res.body);
                            res.body.should.be.a('object');
                            res.body.should.have.property('errors');
                            res.body.errors.should.have.property('pages');
                        });
                });
        } catch (err) {
            console.error(err);
            expect(err).to.have.status(500);
            should.exist(err.body.error);
            throw err;
        };
    });

    it('should return 500 for id not found', () => {
        chai.request(app)
            .get('/v0/estudiantes/1a1a')
            .then(res => { throw res; })
            .catch(err => {
                expect(err).to.have.status(500);
                should.exist(err.body.error);
            });
    });
});

// Modificar
describe('PUT /v0/estudiantes/:id', () => {
    let est = new Estudiante(tempEst);
    it('should return 200', () => {
        try {
            est.save()
                .then(estudiante => {
                    let modEst = tempEst;
                    modEst.nombre1 = "Gabriel";
                    chai.request(app)
                        .put('/v0/estudiantes/'+estudiante._id)
                        .send(modEst)
                        .then(res => {
                            expect(res).to.have.status(200);
                            should.exist(res.body);
                            res.body.should.be.a('object');
                            res.body.should.have.property('errors');
                            res.body.errors.should.have.property('pages');
                            res.body.should.have.property('nombre1').eql('Gabriel');
                        });
                });
        } catch (err) {
            console.error(err);
            expect(err).to.have.status(500);
            should.exist(err.body.error);
            throw err;
        };
    });
    // TODO: Completar todas las posbilidades
    // Dos en especifico
    // 1. ID incorrecto
    // 2. Error de Validacion
});



// Eliminar
describe('DELETE /v0/estudiantes/:id', () => {
    it('should return 200', () => {
        chai.request(app)
            .get('/v0/estudiantes')
            .then(res => {
                expect(res).to.have.status(200);
                should.exist(res.body);
            }).catch(err => { throw err; });
    });

    // TODO: Terminar Eliminar
    // Uno mas en especifico
    // 1. ID incorrecto
});

// Lista
describe('GET /v0/estudiantes', () => {
    it('should return 200', () => {
        chai.request(app)
            .get('/v0/estudiantes')
            .then(res => {
                expect(res).to.have.status(200);
                should.exist(res.body);
                res.body.should.be.a('array');
            }).catch(err => {
                throw err;
            });
    });
});

// 404

describe('GET /v0/404', () => {
    it('should return 404 for non-existent URLs', () => {
        chai.request(app)
            .get('/v0/404')
            .then(res => {
                expect(res).to.have.status(404);
            })
            .catch(err => {
                throw err;
            });
        chai.request(app)
            .get('/v0/notfound')
            .then(res => {
                expect(res).to.have.status(404);
            })
            .catch(err => {
                throw err;
            });
    });
});


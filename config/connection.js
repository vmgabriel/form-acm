
const configDB = {
    uri: "mongodb://127.0.0.1:27017/form-acm",
    options: {
        // Aqui aparecen todas las opciones
        useNewUrlParser: true
    }
};

module.exports = configDB;
